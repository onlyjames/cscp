package CSCPUseThis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Term{
	private String term;
	Map<String, Integer> nextPossible,prePossible; //(Chris)增加prePossible
	int mergeLength= 0;   // 0 meaning no limit,  limit merge length
		public Term(String term){
		this.term = term;
		nextPossible = new HashMap<String, Integer>();
		prePossible = new HashMap<String, Integer>();  //(Chris)增加prePossible
	}
	
	public Term(String term, String next){
		this.term = term;
		nextPossible = new HashMap<String, Integer>();
		addNext(next);
	}
	public void addNext(String s){
		Integer freq = nextPossible.get(s);
		if(s.equals("右")&&freq != null){
			System.out.println(freq);
		}
		if(freq == null){
			nextPossible.put(s, 1);
		}else{
			nextPossible.put(s, freq + 1);
		}
	}
	//(Chris)增加addPrevious
	public void addPrevious(String s){    
		Integer freq = prePossible.get(s);
		if(freq == null){
			prePossible.put(s, 1);
		}else{
			prePossible.put(s, freq + 1);
		}
	}
	
	public void printNextInfo(){
		System.out.println("Term: " + this.term + "------");
		for(String s : nextPossible.keySet()){
			System.out.println("next:" + s + "->" + nextPossible.get(s));
		}
	}
	public void printPreInfo(){
		System.out.println("Term: " + this.term + "------");
		for(String s : prePossible.keySet()){
			System.out.println("pre:"+ s + "->" + prePossible.get(s));
		}
	}
	
	public String getTerm(){
		return term;
	}
	
	@Override
	public boolean equals(Object o){
		return ((Term)o).getTerm().equals(this.term);
	}
	 //正向串聯 取詞
	public ArrayList<String> getMergeTerm(double threshold){
		int denomenater = 0;  //分母 分配機率的分母
		ArrayList<String> result = new ArrayList<String>();
	//	printNextInfo();
		for(String s : nextPossible.keySet()){  //list的下一個節點
			//Chris-----------------------------
			//System.out.print(" nextPossible"+s);
			//----------------------------------
			denomenater += nextPossible.get(s); //加總所有nextPossible次數
		}
		for(String s : nextPossible.keySet()){
			if(s.toCharArray().length>=2){ //nextPossible是兩字以上時
				if(((double)nextPossible.get(s)/denomenater) >= threshold && nextPossible.get(s) > 1){
					//String length can't over 6
					String mergeS=this.term.replaceAll(" ","") + s.replace(" ", "");
					System.out.println("MergeS!!!      -----------"+mergeS);
				
					
					if(!mergeS.equals(this.term)){
						if(mergeS.length()<=mergeLength||mergeLength==0){
					result.add(mergeS);
					}
				}
					}
			}else{							//nextPossible一個字時  出現次數要大於1次 才會連接
				if(((double)nextPossible.get(s)/denomenater) >= threshold && nextPossible.get(s) > 1){
				//長度不能超過6
					String mergeS=this.term.replaceAll(" ","") + s.replace(" ", "");
					System.out.println("MergeS!!!      -----------"+mergeS);
					if(mergeS.length()<=mergeLength||mergeLength==0){
				
					if(!mergeS.equals(this.term)){
						//	if(mergeS.length()<=6){
						result.add(mergeS);}
					
				}
				}
			}
		}
		return result;
	}
//(Chris)增加getPreMergeTerm  逆向串聯取詞
	public ArrayList<String> getPreMergeTerm(double threshold){
		int denomenater = 0;
		ArrayList<String> result = new ArrayList<String>();
		//printPreInfo();
		for(String s : prePossible.keySet()){  //往前面串
			//Chris-----------------------------
			//System.out.print(" prePossible"+s);
			//----------------------------------
			denomenater += prePossible.get(s); //加總所有prePossible次數
		}
		
		for(String s : prePossible.keySet()){
			
			if(s.toCharArray().length>=2){ //prePossible是兩字以上時
			
			//	System.out.println(this.term+"    "+s+"   "+prePossible.get(s));
				
				if(((double)prePossible.get(s)/denomenater) >= threshold && prePossible.get(s) > 1){
					String mergeS=s.replace(" ", "")+this.term.replaceAll(" ","");
					if(mergeS.length()<=mergeLength||mergeLength==0){
					if(!mergeS.equals(this.term)){
						//	if(mergeS.length()<=6){
						result.add(mergeS);}
					
				}
				}
			}else{							//prePossible一個字時
				
				//System.out.println(this.term+"    "+s+"   "+prePossible.get(s));
				
				
				if(((double)prePossible.get(s)/denomenater) >= threshold && prePossible.get(s) > 1){
					String mergeS=s.replace(" ", "")+this.term.replaceAll(" ","");
					if(mergeS.length()<=mergeLength||mergeLength==0){
					if(!mergeS.equals(this.term)){
						//	if(mergeS.length()<=6){
						result.add(mergeS);}
					
				 }
				}
			}
		}
		return result;
	}
}
