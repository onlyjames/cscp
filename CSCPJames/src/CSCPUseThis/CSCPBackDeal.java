package CSCPUseThis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CSCPBackDeal {
	Map<String, Integer> ans;
	Map<String, Integer> removes = new HashMap<String, Integer>();
	private static CSCPBackDeal instence = new CSCPBackDeal();

	private CSCPBackDeal() {
	}

	public static CSCPBackDeal getInstence() {
		return instence;
	}

	public Map<String, Integer> backDeal(Map<String, Integer> data) {
		ans = data;
		removes.clear();
		for (String key : data.keySet()) {
			// System.out.println(key);
			if (key.equals("		")) {
				removes.put(key, data.get(key));
			}
			for (String ansKey : ans.keySet()) {
				if (ansKey.contains(key)) {
					if (ans.get(ansKey) > data.get(key)) {
						removes.put(key, data.get(key));// 加入要移除的清單  次數較少
					} else if (ans.get(ansKey) == data.get(key) && ansKey.length() > key.length()) {
						removes.put(key, data.get(key)); // 加入要移除的清單   次數一樣
					}
				}
			}
		}
		// 移除
		for (String key : removes.keySet()) {
			ans.remove(key, removes.get(key));
		}

		return ans;
	}
}
