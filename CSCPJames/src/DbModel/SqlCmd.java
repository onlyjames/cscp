package DbModel;



import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;


public abstract class SqlCmd {

	public DbManager dbMgr;
	ResultSet resultSet;
	public Connection conn = null;
	Statement sta = null;
	public ResultSet rs = null;
	public PreparedStatement pst = null;
	Object object;

	public SqlCmd() {
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public boolean execute() {
		try {
			openDB();
			queryDB(); 
			object = processResult(); 
			disconnectDB(); 
			return true; // 確認執行成功

		} catch (Exception e) {
			// disconnectDB();

			e.printStackTrace();
		}
		return false;
	}

	public abstract Object processResult();

	public abstract void disconnectDB();
	// TODO Auto-generated method stub
	// Close
	// dbMgr.closeDBBatch(pst, conn);
	// dbMgr.closeDB(sta, conn);

	public void openDB() { 
		try {
			dbMgr = DbManager.getDbInstance(); // 這邊會拿到Insatnce
			conn = (Connection) dbMgr. DBinit(); // 取得資料庫連結
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}
	}

	public abstract void queryDB();

}
