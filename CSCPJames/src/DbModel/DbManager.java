package DbModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbManager {
	private static volatile DbManager instance = null;

	private DbManager() {
	}

	public static DbManager getDbInstance() {
		if (instance == null) {
			synchronized (DbManager.class) {
				if (instance == null) {
					instance = new DbManager();
				}
			}
		}
		return instance;
	}

	public Connection DBinit() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String sql = "jdbc:mysql://140.125.84.41:3306/" + "penguintest"
					+ "?useUnicode=true&characterEncoding=utf8&user=root&password=root";
			System.out.println(sql);
			conn = (Connection) DriverManager.getConnection(sql);
			// conn =
			// DriverManager.getConnection("jdbc:mysql://140.125.84.41:3306/" +
			// databaseName
			// +
			// "?useUnicode=true&characterEncoding=big5&user=root&password=root");
		} catch (ClassNotFoundException e) {
			System.err.println("MySqlConnection Error: " + e.getMessage());
		} catch (SQLException e) {
			System.err.println("MySqlConnection Error: " + e.getMessage());
		}
		return conn;
	}

	public void closeDB(ResultSet rs, PreparedStatement pst, Connection conn) {
		try {
			rs.close();
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void closeDBBatch(PreparedStatement pst, Connection conn) {
		try {
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
