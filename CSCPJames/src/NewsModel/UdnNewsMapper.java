package NewsModel;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UdnNewsMapper {
	ResultSet set;
	List<UdnNews> resultList;
	public UdnNewsMapper(ResultSet set){
		this.set = set;
		this.resultList = new ArrayList<UdnNews>();
	}
	
	public List<UdnNews> getUdnNewsList() throws SQLException{
		while(set.next()){
			UdnNews news = new UdnNews();
			news.setId(set.getInt("id"));
			news.setContent(set.getString("content"));
			news.setTitle(set.getString("title"));
			news.setUrl(set.getString("url"));
			news.setCaption(set.getString("caption"));
			news.setDate(set.getDate("date"));
			news.setImageURL(set.getString("imageURL"));
			resultList.add(news);
		}
		return resultList;
	}
}
