package DbModel;

import java.util.List;

import NewsModel.UdnNews;
import ReadDBModel.ReadFinance;
import ReadDBModel.ReadMix;

public class DbInvoker {
	public List<UdnNews> readFinance() {
		SqlCmd sqlCmd = new ReadFinance();
		sqlCmd.execute();
		return (List<UdnNews>) sqlCmd.getObject();
	}

	public List<UdnNews> readMix() {
		SqlCmd sqlCmd = new ReadMix();
		sqlCmd.execute();
		return (List<UdnNews>) sqlCmd.getObject();
	}
}
