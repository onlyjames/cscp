package ReadDBModel;

import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import DbModel.SqlCmd;

public abstract class ReadCommand extends SqlCmd {

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

	}

	public void disconnectDB() {
		dbMgr.closeDB(rs, pst, conn);
	}
}