package CSCPUseThis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSCP {
	private String content;
	private double threshold;
	private List<Term> terms;
	private ArrayList<String> mutiDoc = new ArrayList<String>();
	Map<String, Integer> result = new HashMap<String, Integer>();

	public CSCP() {
		terms = new ArrayList<Term>();
	}

	public void setSingleContentThreshold(String content, double threshold) {  //設�?? ?��??�件??? ?���? 以�?��?檻�??
		this.content = content;
		this.threshold = threshold;
		terms.clear();
	}

	public void setMutiContentThreshold(ArrayList<String> inMutiDoc, double threshold) { //設�?��?��?�件??�內容以??��?檻�??
		mutiDoc = inMutiDoc;
		content = "";
		for (int i = 0; i < mutiDoc.size(); i++) {// ??�單篇�?��?�串??��?大�?��?��??
			content += mutiDoc.get(i);
		}
		this.threshold = threshold;
		terms.clear();
	}

	public Map<String, Integer> runCSCP() {  //跑單??�件??�方�?
		Map<String, Integer> result = new HashMap<String, Integer>();
		String[] temp = preProcessing();
		/*
		 * for(int i=0;i<temp.length;i++){ System.out.println(temp[i]); }
		 */

		iteration(temp);

		postProcessing();

		for (Term t : terms) { // 使用spilt計�?��?�頻
			String content = "000" + this.content + "000";
			try {
				int k = content.split(t.getTerm()).length - 1;
				System.out.println(t.getTerm() + "   " + k);
				//result.put(t.getTerm(), content.split(t.getTerm()).length - 1);
			result.put(t.getTerm(), k);
			} catch (Exception e) {
				System.out.println("Spilt Has Exception");
			}
		}
		CSCPBackDeal backDeal = CSCPBackDeal.getInstence(); // 後�?��?�instence
		return (Map<String, Integer>) backDeal.backDeal(result); // 使用後�?��??
	}

	public Map<String, Integer> runMutiCSCP() { // 跑�?��?�件??�方�? ??�傳 AllVocabuary ?��?��??��?�以??? 詞頻
		Map<String, Integer> allVoResult = new HashMap<String, Integer>();
		String[] temp = preProcessing();
		/*
		 * for(int i=0;i<temp.length;i++){ System.out.println(temp[i]); }
		 */

		iteration(temp);

		postProcessing();

		for (Term t : terms) { // 使用spilt來�?��?��?�頻
			String content = "000" + this.content + "000";
			try {
				int k = content.split(t.getTerm()).length - 1;
				System.out.println(t.getTerm() + "   " + k);
				result.put(t.getTerm(), content.split(t.getTerm()).length - 1);
			} catch (Exception e) {
				System.out.println("Spilt Has Exception");
			}
		}
		CSCPBackDeal backDeal = CSCPBackDeal.getInstence(); // ??��?��?��?��?��?�instence
		result = (Map<String, Integer>) backDeal.backDeal(result); // 使用後�?��??
		return result;
	}

	public ArrayList<Map<String, Integer>> backTrackMutiDoc() {// 要先執行
																// runMutiCSCP
		ArrayList<Map<String, Integer>> eachDoc = new ArrayList<Map<String, Integer>>();
		Map<String, Integer> singleDoc;
		for (int i = 0; i < mutiDoc.size(); i++) { // 回追文件
			singleDoc = new HashMap<String, Integer>();
			for (String k : result.keySet()) {
				int count = countVocabuary(k, mutiDoc.get(i)); // 計�?? 詞在?��篇�?��?? ?��?��幾次
				if (count != 0) {// 詞頻?��0 就�?�能寫入此�?��?��?�斷詞�?��??
					singleDoc.put(k, count);
				}
			}
			eachDoc.add(singleDoc);
		}
		return eachDoc;
	}

	protected String[] preProcessing() { //前處理 斷句
		// 過濾標點符號
		content = content.replaceAll("[\\p{P}+~$`^=|<>～｀＄＾＋＝｜＜＞￥×]", " ").replaceAll("[0-9]", "").replaceAll("[a-zA-Z]",
				"");

		System.out.println("content:" + content);

		String[] sentences = content.split(" ");

		for (String s : sentences) {
			System.out.println("sentences:" + s);
			System.out.println("-------------------------");
		}
		return sentences;
	}

	// paper's step2 建立有向圖  串聯list
	protected void iteration(String[] sentences) {

		
		
		// 建立有向圖
		buildGraph(sentences);
		// 正向串連
		forwardGive();
		// 逆向串連
		backWardGive();
		postProcessing(); // Back deal   detect space char and length==1 String 
		// --------------------------------------------------------------*/
	}

	// Build list 
	private void buildGraph(String[] sentenceArray) {
		for (int j = 0; j < sentenceArray.length; j++) {
			String sentence = sentenceArray[j];
			for (int i = 0; i < sentence.length(); i++) {
				boolean isContain = false;
				System.out.println("Identify Char");
				System.out.println("substring " + sentence.substring(i, i + 1));
				Term t = new Term(sentence.substring(i, i + 1));

				for (Term term : terms) { // judge term is in terms if inside then can't add again
					if (t.equals(term)) {
						isContain = true;
						t = term;
						break;
					}
				}
				// use if to avoid OutOfIndex
				try {

					if ((i + 2) <= sentence.length()) {  //將�?��?��??��??�串起�?? ??��??
						// System.out.println("sentence.length() "
						// +sentence.length());
						// addNext is mean add in front of it
						t.addNext(sentence.substring(i + 1, i + 2));
					}
					if ((i - 1) != -1) { // addRervious is mean add in back of it
						t.addPrevious(sentence.substring(i - 1, i));
					}
				} catch (Exception e) {
					e.getMessage();
				}
				// if term is's in TermsList. Add it to TermsList
				if (!isContain) {
					terms.add(t);
				}
			}
		}
		// ?��?�� terms
		/*
		 * for (Term k : terms) { System.out.println("term " + k.getTerm());
		 * k.printNextInfo(); k.printPreInfo(); }
		 */
	}

	private void forwardGive() {
		// add new Vocabuary in forward
		// ------------------------------------
		int end = terms.size();
		for (int i = 0; i < end; i++) {
			Term temp = terms.get(i);
			System.out.println("terms.get(i)"+"  "+i+"   " + temp.getTerm());

			
			
			for (String s : temp.getMergeTerm(threshold)) { // getMergeTerm
															// The String has been merged

				System.out.println("merging" + s);
				Term t = new Term(s);

				
				// System.out.println("s is"+ s);
				String[] contentArray = content.split(s); // split String in all content To find String location
										 
				try {
					for (int index = 0; index < contentArray.length; index++) {

						// System.out.println("s is"+ s);
						// System.out.println("content is"+ content);

						if (!contentArray[index].equals("")) {// 不�?�串?��空白
							// System.out.println(t.getTerm() + " -> " + s1);

							if (!contentArray[index].equals(" ") && index != 0) {  //spilt 第�??��?��?��?�是s??��?�到"" 不是s??��?�串?��?��??��?��?��?�面
								t.addNext(contentArray[index].substring(0, 1));
							}
							if (index != contentArray.length - 1) {  //??�為?��spilt 
								if (!contentArray[index]
										.substring(contentArray[index].length() - 1, contentArray[index].length())
										.equals(" ")) {// 不�?�串?��空白
									t.addPrevious(contentArray[index].substring(contentArray[index].length() - 1,
											contentArray[index].length()));
								}
							}
						}
					}
				} catch (Exception e) {
					System.out.println("s is" + s);
					System.out.println("content is" + content);
				}

				terms.add(t);
				end++;
			
				}
			

		}
		/*
		 * for (Term k : terms) { System.out.println("term " + k.getTerm()); //
		 * k.printNextInfo(); // k.printPreInfo(); }
		 */
	}

	private void backWardGive() {
		// backward new Vocabuary 
		//// --------------------------------------------------

		int end = terms.size();
		for (int i = 0; i < end; i++) {
			Term temp = terms.get(i);
			System.out.println("terms.get(i)" + temp.getTerm());
			
			for (String s : temp.getPreMergeTerm(threshold)) { // getMergeTerm?��已�?��?�併完�?�string
				System.out.println("prePossiblemerging" + s);
				Term t = new Term(s);

				int judge = 0;
				for (int j = 0; j < terms.size(); j++) { // judge term is in list裡面
					if (t.equals(terms.get(j))) {
						judge = 1;
					}
				}
				
				
				if (judge != 1) {
					try {
						String[] contentArray = content.split(s);
						for (int ii = 0; ii < contentArray.length; ii++) {
							if (!contentArray[ii].equals("")) {
								if (!contentArray[ii].substring(0, 1).equals(" ") && ii != 0) { // 不�?�串?��空白
									System.out.println("Sub  :" + contentArray[ii].substring(0, 1));
									t.addNext(contentArray[ii].substring(0, 1));
								}
								if (ii != contentArray.length - 1) {
									if (!contentArray[ii]
											.substring(contentArray[ii].length() - 1, contentArray[ii].length())
											.equals(" ")) {// 不�?�串?��空白
										t.addPrevious(contentArray[ii].substring(contentArray[ii].length() - 1,
												contentArray[ii].length()));
									}
								}
							}
						}
					} catch (Exception e) {
						System.out.println("s is " + s);
						System.out.println("content is " + content);
					}

					terms.add(t);
					end++;
				}
			
			}
			

		}
		/*
		 * for (Term k : terms) { System.out.println("term " + k.getTerm());
		 * k.printNextInfo(); k.printPreInfo(); }
		 */
	}

	protected void postProcessing() {

		for (int i = 0; i < terms.size(); i++) {
		//	System.out.println("term::::"+terms.get(i).getTerm()+"count:::"+terms.get(i).getTerm().length());
			if (terms.get(i).getTerm().length() == 1) {
				terms.remove(i);
				i--;
			}
		}

		for (Term k : terms) {
			System.out.println("term " + k.getTerm());
			//k.printNextInfo();
			//k.printPreInfo();
		}

	}

	public int countVocabuary(String vocabuary, String content) { // 計�?? 詞頻 多�?�件??�追
																	// 要�?��?�單篇�?�件??��?�頻
		int ans = 0;
		String[] contentArray = content.split("");
		String[] vocabuaryArray = vocabuary.split("");
		int i = 0;
		while (i < contentArray.length) {

			for (int j = 0; j < vocabuaryArray.length; j++) {
				// System.out.println(contentArray[i+j]+" "+vocabuaryArray[j]);
				if (j == (vocabuaryArray.length - 1) && contentArray[i + j].equals(vocabuaryArray[j])) {
					ans++;
				}
				if (contentArray[i + j].equals(vocabuaryArray[j])) {
					continue;
				} else {
					break;
				}
			}
			i++;
		}
		return ans;
	}

	// 測試�???
	public static void main(String[] argu) {
	//	String s="林又倉系統公司林又倉系統公司，好久好久好久,文章玟張";
		 String s="最新的消費者物價指數出爐，五月的總體物價比上個月上漲了0.14％，其中蔬菜的價格漲幅最大，高達一成，醫藥消費則是略降了0.06%，在薪資表現部份，工業和服務業月平均薪資所得比上月多了一元，製造業則是下滑2.81元。";
		 		 /*
		 * CSCP test = new CSCP(); test.setSingleContentThreshold(s, 0.5);
		 * Map<String, Integer> result = test.runCSCP(); CSCPBackDeal CBD =
		 * CSCPBackDeal.getInstence(); result = CBD.backDeal(result);
		 * System.out.println("result----------------"); for (String k :
		 * result.keySet()) { // System.out.println("pass");
		 * System.out.println(k + ":" + result.get(k)); }
		 */
		CSCP test = new CSCP();
		test.setSingleContentThreshold(s, 0.1);
		CSCPBackDeal BD=CSCPBackDeal.getInstence();
		Map<String, Integer> ans=BD.backDeal(test.runCSCP());
		for(String k:ans.keySet()){
			System.out.println(k+"count:::"+ans.get(k));
		}
		/*
		test.content = s;
		test.threshold = 0.8;
		test.preProcessing();
		test.buildGraph(test.preProcessing());
		test.forwardGive();
		
		test.backWardGive();
		test.postProcessing();*/
	}
}
