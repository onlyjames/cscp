package MainController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import CSCPUseThis.*;
import DbModel.DbInvoker;
import NewsModel.UdnNews;
import NewsModel.UdnNewsMapper;
import TxtModel.Writetxt;

public class Main {
	public static void main(String[] argu) throws Exception {
		DbInvoker dbI = new DbInvoker();
		 new Main().SingleCSCPDataFinish(0.1, 0.9, 0.1, "D:/CSCPData/CSCPSingle/resultFinance", dbI.readFinance()); //
		// 路徑檔名別寫死
		// 別加txt
		 new Main().SingleCSCPDataFinish(0.1, 0.9, 0.1, "D:/CSCPData/CSCPSingle/resultMix", dbI.readMix());

		// new Main().MutiCSCPFinish(0.5, 0.9, 0.1,
		// "D:/CSCPDATA/CSCPMuti/resultFinance", dbI.readFinance()); // 路徑檔名別寫死
		// 別加txt
		// new Main().MutiCSCPFinish(0.5, 0.9, 0.1,
		// "D:/CSCPDATA/CSCPMuti/resultMix", dbI.readMix()); // 路徑檔名別寫死
		// 別加txt

		// test area
		//new Main().SingleCSCPDataFinish(0.1, 0.9, 0.1, "D:/CACPTESTCASE/Single/resultFinance", dbI.readFinance());

	}

	public void MutiCSCPFinish(double start, double end, double tolerance, String root, List<UdnNews> list)
			throws Exception {
		Map<UdnNews, Map<String, Integer>> toTxtResult = new HashMap<UdnNews, Map<String, Integer>>();
		// Writetxt ww = new Writetxt(); // 寫入記事本
		Writetxt ww = Writetxt.getInstance();
		ArrayList<String> mutiString = new ArrayList<String>();
		// conn.setSql("select * from 201406_08_random_yahoo_news_finance_6000
		// Where id<23300");
		List<UdnNews> newsList = list;
		// 從DB取出新聞LIST

		for (UdnNews k : newsList) {
			mutiString.add(k.getContent());
		}
		CSCP CSCPMain = new CSCP();

		// Compute executing time
		long time1, time2;
		double time3;
		Map<String, Double> timeRecord = new HashMap<String, Double>();

		double tran = start;
		while (tran <= end) {
			time1 = System.currentTimeMillis(); // start time

			CSCPMain.setMutiContentThreshold(mutiString, tran);// set cscp
			Map<String, Integer> result = CSCPMain.runMutiCSCP(); // run cscp

			ww.setRoot(root + "AllVo" + String.valueOf(tran) + ".txt");
			ww.writeMutiAllDoc(result);
			ArrayList<Map<String, Integer>> bkSingleDoc = CSCPMain.backTrackMutiDoc(); // backTrack
																						// Muti
			Map<UdnNews, Map<String, Integer>> TxtResult = new HashMap<UdnNews, Map<String, Integer>>();
			for (int i = 0; i < newsList.size(); i++) {
				// System.out.println(bkSingleDoc.get(i));
				TxtResult.put(newsList.get(i), bkSingleDoc.get(i));
			}
			time2 = System.currentTimeMillis(); // end time
			time3 = (double) (time2 - time1) / 1000; // Spend time
			timeRecord.put(String.valueOf(tran), time3); // put in to map

			ww.setRoot(root + String.valueOf(tran) + ".txt");
			ww.writeSingleDoc(TxtResult);
			// print now and end rate
			System.out.println(tran + "   " + end);
			tran += tolerance;
			tran = Math.floor(tran * 100) / 100;
		}
		ww.setRoot(root + "TimeRecord.txt");
		ww.writeTime(timeRecord);

	}

	public void SingleCSCPDataFinish(double start, double end, double tolerance, String root, List<UdnNews> list)
			throws Exception {
		Map<UdnNews, Map<String, Integer>> toTxtResult = new HashMap<UdnNews, Map<String, Integer>>();
		// Writetxt ww = new Writetxt(); // 寫入記事本
		Writetxt ww = Writetxt.getInstance();
		List<UdnNews> newsList = list;
		// 從UdnNewsMapper取出
		// 物件UdnNews的list
		// System.out.println(newsList.get(0).getContent());
		CSCP CSCPMain = new CSCP();

		// Compute executing time
		long time1, time2;
		double time3;
		Map<String, Double> timeRecord = new HashMap<String, Double>();

		double tran = start;
		while (tran <= end) {
			time1 = System.currentTimeMillis(); // start time

			for (UdnNews k : newsList) {
				CSCPMain.setSingleContentThreshold(k.getContent(), tran);

				Map<String, Integer> result = CSCPMain.runCSCP();
				
				
				toTxtResult.put(k, result); // 要寫進Txt 的傳遞數
				// System.out.println(tran+" "+end);

				for (String key : result.keySet()) {
					System.out.println(key + "  " + result.get(key));
				}
			}

			ww.setRoot(root + String.valueOf(tran) + ".txt");
			ww.writeSingleDoc(toTxtResult);

			time2 = System.currentTimeMillis(); // end time
			time3 = (double) (time2 - time1) / 1000; // Spend time
			timeRecord.put(String.valueOf(tran), time3); // put in to map

			System.out.println(tran + "   " + end);
			tran += tolerance;
			tran = Math.floor(tran * 100) / 100;
			toTxtResult.clear();
		}
		ww.setRoot(root + "TimeRecord.txt"); // write time to txt
		ww.writeTime(timeRecord);
	}
}
