package TxtModel;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import CKIP.CKIP;
import NewsModel.UdnNews;

public class Writetxt {
	String root;
	CKIP ck;
	private static Writetxt instance = null;

	private Writetxt() {
	}

	public static Writetxt getInstance() {
		if (instance == null) {
			synchronized (Writetxt.class) {
				if (instance == null) {
					instance = new Writetxt();
				}
			}
		}
		return instance;
	}

	public void setRoot(String inRoot) {
		root = inRoot;
	}

	public void writeTime(Map<String, Double> all) throws Exception { // 寫時間
		BufferedWriter writer = new BufferedWriter(new FileWriter(root));
		for (String k : all.keySet()) {
			System.out.println("writeTime");
			writer.write("Rate " + k + ":" + all.get(k));
			writer.newLine();
			writer.flush();

		}

		writer.close();
		this.ck = new CKIP();
	}

	public void writeMutiAllDoc(Map<String, Integer> allVo) throws IOException { // 多文件總詞頻會用到
		BufferedWriter writer = new BufferedWriter(new FileWriter(root));
		for (String k : allVo.keySet()) {
			System.out.println("write all Vocabuary");
			writer.write("CSCP斷詞:  " + k + ":" + allVo.get(k));
			writer.newLine();
			writer.flush();
		}

		writer.close();
	}

	public void writeSingleDoc(Map<UdnNews, Map<String, Integer>> inResult) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(root));

		for (UdnNews key : inResult.keySet()) {
			System.out.println("Write Single Vocabuary");
			writer.write("id:" + key.getId());
			// test ,  coding
			/*  NOT 物價
			if(key.getId()==26084){
				Map<String, Integer> vocabuaryTest = inResult.get(key);
				for (String k : vocabuaryTest.keySet()){
					System.out.println(k);
					System.out.println(vocabuaryTest.get(k));
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
			//end
			writer.newLine();
			writer.write("content:" + key.getContent());
			Map<String, Integer> vocabuary = inResult.get(key);
			writer.newLine();
			// Map<String,String> ckipresult = new HashMap<String,String>();
			for (String k : vocabuary.keySet()) {
				/*
				 * if(k.equals("物價")){ System.out.println("write to txt  "+k);
				 * try { Thread.sleep(10000); } catch (InterruptedException e) {
				 * // TODO Auto-generated catch block e.printStackTrace(); } }
				 */

				synchronized (Writetxt.class) {
					writer.write("CSCP斷詞:  " + k + ":" + vocabuary.get(k));
					writer.newLine();
					writer.flush();
				}
				// String s = ck.getCKIPString(k);
				System.out.println("test````````````````````" + k);
			}
			/*
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			writer.write("----------------------------");
			writer.newLine();
			writer.flush();
		}

		writer.close();
	}
}
